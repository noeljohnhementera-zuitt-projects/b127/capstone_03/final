import React from 'react';

import { Link } from 'react-router-dom';

export default function Footer () {
	return (
		<div>
			<footer className="bg-warning text-black pt-5">
			    <div className="container-fluid text-center text-md-left">
			        <div className="row">
			            <div className="col-md-6 mt-md-0 mt-3 text-center">
			            <div className='footer-padding logo-footer'>
			            	<i class="fas fa-utensils"></i>
			            </div>
			                <h3 className="text-uppercase">All Resto E-Commerce</h3>
			                <p>Your Go-to Online Resto</p>
			                <div>
			                	<a href="mailto:noeljohnhementera97@gmail.com">noeljohnhementera97@gmail.com</a>
			                </div>
			                <div>
			                	<a href="tel:0449409431" target="_blank">044-940-9431</a>
			                </div>
			                <div>
			               		<p>Nueva Ecija, Philippines</p>
			                </div>
			            </div>

			            <hr className="clearfix w-100 d-md-none pb-0"/>

			            <div className="col-md-3 mb-md-0 mb-3">
			                <h5 className="text-uppercase">Quick Links</h5>
			                <ul className="list-unstyled">
			                    <li><Link to={'/'}>Home</Link></li>
			                    <li><Link to={'/products'}>Products</Link></li>        
			                </ul>
			            </div>

			            <div className="col-md-3 mb-md-0 mb-3">
			                <h5 className="text-uppercase">Socials</h5>
			                <ul className="list-unstyled">
			                    <li><a href="https://linkedin.com/in/noel-john-hementera-b2a11312b/" target="_blank">LinkedIn</a></li>
			                    <li><a href="https://join.skype.com/invite/WvcrJ9rdJnMY" target="_blank">Skype</a></li>
			                </ul>
			            </div>
			        </div>
			    </div>

			    <div className="footer-copyright text-center py-3">© 2021 Copyright:
			        <Link to={'/'}> All Resto E-Commerce</Link>
			    </div>

			</footer>
		</div>
	)
}
