import { Link } from 'react-router-dom';

import { Jumbotron, Container, Button, Col, Row } from 'react-bootstrap';

export default function Error () {
	return (
	<Container className='padding-top-5'>
		<Jumbotron>
			<h1>Page Not Found</h1>
			<p>Browse our product collection in our <Link to="/products">products page</Link>.</p>
		</Jumbotron>
	</Container>
	)
}