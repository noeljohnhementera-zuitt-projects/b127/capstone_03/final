import { useContext, useState, useEffect } from 'react';

import { Container } from 'react-bootstrap';

import { Redirect } from 'react-router-dom';

import GlobalDataContext from '../GlobalDataContext';

import UserOrders from '../components/UserOrders';

export default function MyOrders () {
		// useContext() to determine if user is admin or not
		const { user } = useContext(GlobalDataContext);

		// fetched data will be stored in this useState()
		const [ allProducts, setAllProducts ] = useState([])

		const getUserOrder = () =>{
		    fetch('https://mighty-sands-70512.herokuapp.com/orders/check-user-order', {
		    	method: 'GET',
		    	headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
				}
		    })
		    .then(res => res.json())
		    .then(data => {
		        setAllProducts(data)
		      })
		    //setProducts(getUserOrder)
		  }

		// to reuse the getAllProducts() function and avoid hard refresh
		useEffect(()=>{
			getUserOrder()
		}, [])

	return (
		<Container>
			{	
				(user.customerAccessToken !== null) ?
				<UserOrders fetchedData={allProducts}/>
				:
				<Redirect to = "/login" />
			}	
		</Container>
	)
}
//productData is a paramameter to pass the new value of getter


/*// Bootstrap
import { Table, Button, Container } from 'react-bootstrap';

import { useParams, Link, Redirect } from 'react-router-dom';

import GlobalDataContext from '../GlobalDataContext';

import { Fragment, useEffect, useState, useContext } from 'react';

export default function MyOrders () {

	const { user } = useContext(GlobalDataContext);

	const [ products, setProducts ] = useState([]);

	const [ customerId, setCustomerId ] = useState('');
	const [ productId, setProductId ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);
	const [ totalAmount, setTotalAmount ] = useState(0);
	const [ date, setDate ] = useState('');

	const getUserOrder = (props) =>{
    fetch('http://localhost:4000/orders/check-user-order')
    .then(res => res.json())
    .then(data => {
        setProducts(data)
        console.log(data)
      })
    setProducts(getUserOrder)
  }

 
	useEffect(()=>{
		const fetchProducts = getUserOrder.map(eachProduct =>{
			return (
			<tr	key={eachProduct._id}>
				<td>{eachProduct._id}</td>
				<td>{eachProduct.customerId}</td>
				<td>{eachProduct.productId}</td>
				<td>{eachProduct.price}</td>
				<td>{eachProduct.quantity}</td>
				<td>{eachProduct.totalAmount}</td>
				<td>{eachProduct.date}</td>
			</tr>
			)
		})

		setProducts(fetchProducts)
	}, [getUserOrder])

	return (
			(user.customerAccessToken === null) ?
				<Redirect to = "/logout" />
				:
		<Fragment>
				<div className='my-4 text-center'>
					<h2>Order Transaction</h2>
				</div>
				<Container>
					<Table striped bordered hover responsive>
						<thead className="bg-dark text-white">
							<tr className='text-center'>
								<th>Customer ID</th>
								<th>Product ID</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Total Amount</th>
								<th>Date</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							{products}
						</tbody>
					</Table>
				</Container>
		</Fragment>
	)
}*/