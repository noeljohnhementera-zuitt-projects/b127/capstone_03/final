// First Step Imports
import { Fragment } from 'react';

import { Container } from 'react-bootstrap';

// Components
import CardHighlights from '../components/CardHighlights';
import SiteJumbotron from '../components/SiteJumbotron';
import Reviews from '../components/Reviews';
import Banner from '../components/Banner';

export default function Home() {

	return (
		<Fragment>
			< Banner />
			< CardHighlights />
				{/*< Featured />*/}
			< Reviews />
		</Fragment>
	)
}