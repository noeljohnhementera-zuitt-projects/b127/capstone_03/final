import { useContext, useState, useEffect } from 'react';

import { Container } from 'react-bootstrap';

import GlobalDataContext from '../GlobalDataContext';

import { Redirect } from 'react-router-dom';

import CartView from '../components/CartView';

export default function Cart () {
		// useContext() to determine if user is admin or not
		const { user } = useContext(GlobalDataContext);

		// fetched data will be stored in this useState()
		const [ allProducts, setAllProducts ] = useState([])
		const [ customerId, setCustomerId ] = useState('')

		/*const getProductsInCart = () =>{
			fetch('http://localhost:4000/cart/check-cart', {
				method: 'GET',
		    	headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setAllProducts(data)
				console.log(data)
				// to check if its working
			})
		}*/

		const getProductsInCart = () =>{
			fetch('https://mighty-sands-70512.herokuapp.com/cart/all', {
				method: 'GET',
		    	headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setAllProducts(data)
				console.log(data)
				// to check if its working
			})
		}

		// to reuse the getAllProducts() function and avoid hard refresh
		useEffect(()=>{
			getProductsInCart()
		}, [])

	return (
		<Container>
			{	
				(user.customerAccessToken !== null) ?
				<CartView  productData={allProducts} getProductsInCart={getProductsInCart}/>
				:
				<Redirect to = "/login" />
			}	
		</Container>
	)
}
//productData is a paramameter to pass the new value of getter