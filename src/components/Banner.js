// Bootstrap
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function Banner() {
	return (
	<div className='jumbotron-home text-white'>
		<h1 className='display-1 align-banner-h1'>All Resto E-Commerce</h1>
		<h3 className='align-banner-h3'>Your Go-To Online Resto</h3>
		<div className='align-banner-button'>
			<Link className='btn btn-primary-home-products mx-3' to={'/products'}>Browse Our Products</Link>
				{/*<Button variant='primary' className='mx-3' href="#front">Featured Products</Button>*/}
		</div>	
	</div>
		
		)
}


	/*<div>
			<Row>
				<Col>
					<Jumbotron className='jumbotron text-center text-white'>
						<h1 className='align display-1'>All Resto E-Commerce</h1>
						<h3>Your Go-To Online Resto</h3>
						<Row>
							<Col>
								<Link className='btn btn-primary mx-3' to={'/products'}>Browse our Products</Link>
	
							</Col>
						</Row>
					</Jumbotron>
				</Col>
			</Row>
		</div>*/