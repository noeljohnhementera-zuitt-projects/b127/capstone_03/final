import { useState, useEffect, Fragment } from 'react';

import OrderCard from './OrderCard'

// Products = parent (passes data)
// UserView = child (uses the data)
export default function UserOrders ( { fetchedData } ) {

	const [ products, setProducts ] = useState([])
	console.log(fetchedData)

	useEffect(() =>{

		const getAllProducts = fetchedData.map(eachProduct =>{
			if(eachProduct){
				return (
					<Fragment>
						<OrderCard productProp={eachProduct} key={eachProduct._id}/>
						{/*productProp will be used in child component, ProductCard*/}
					</Fragment>
				)
			}else{
				return null
			}
		})

		setProducts(getAllProducts)

	}, [fetchedData])

	return (
		<Fragment>
			{products}
		</Fragment>
	)
}
// setter setProducts(getAllProducts) wc was set in useEffect() will be the new value of the getter, products