import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form, Container } from 'react-bootstrap';

import { Link } from 'react-router-dom';

import Swal from 'sweetalert2';
// Products = parent (passes data)
// AdminView = child (uses the data)
export default function CartView (props) {

	// use the data from parent component, Products
	const { productData, getProductsInCart } = props
	console.log(props)

	const [ products, setProducts ] = useState([])

	// addProduct -> create useState() to store data when adding a product
	//const [ name, setName ] = useState('');
	const [ productName, setProductName ] = useState('');
	const [ quantity, setQuantity ] = useState('');
	const [ totalAmount, setTotalAmount ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ productId, setProductId ] = useState('');
	const [ firstName, setFristName ] = useState('');
	const [ lastName, setLastName] = useState('')
	const [ cartId, setCartId ] = useState('');

	// updateProductButton -> useState() for updateProductButton modal button
	const [ showEditProductModal, setShowEditProductModal ] = useState(false);
	// openEditModal -> useState() to get the fetched data and become the setter or new value of the _id
	const [ showProductModal, setShowProductModal ] = useState(false);
	const openProductModal = () => setShowProductModal(true)
	const closeProductModal = () => setShowProductModal(false)
	// updateProductButton -> functions handling opening and closing of updateProductButton modal
	const openEditModal = (productId) =>{
		fetch(`https://mighty-sands-70512.herokuapp.com/cart/${ productId }`)
		.then(res => res.json())
		.then(data =>{
			// use setter in useState to change the update the properties of an object
			setProductId(data._id)
			setPrice(data.price)
			setQuantity(data.quantity)
			setTotalAmount(data.totalAmount)
			setProductName(data.productName)
		})
		// open the modal
		setShowEditProductModal(true)
	}

	const closeEditModal = () =>{
		// close the modal
		setShowEditProductModal(false)
	}

	const deleteProduct = (cartId) => {
		fetch(`https://mighty-sands-70512.herokuapp.com/cart/${cartId}/deleted`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			getProductsInCart()
			setCartId(data._id)
		})
	}

	useEffect(()=>{
		const fetchProducts = productData.map(eachProduct =>{
			return (
			<tr	key={eachProduct._id} className='text-center'>
				<td>{eachProduct._id}</td>
				<td>{eachProduct.productId}</td>
				<td>{eachProduct.productName}</td>
				<td>{eachProduct.price}</td>
				<td>{eachProduct.quantity}</td>
				<td>{eachProduct.totalAmount}</td>
				<td>
					<Container className='d-flex justify-content-center'>
						<Button className='mx-2 primary-button-modal' variant='primary' size='sm' onClick={() => openEditModal(eachProduct._id)}>Update Quantity</Button>
						<Button className='mx-2 primary-button-modal' variant='primary' size='sm' 
								onClick={ () => { 
									openProductModal(eachProduct._id);
									deleteProduct(eachProduct._id);
								}}>Purchase</Button>
					</Container>
				</td>
			</tr>
			)
		})

		setProducts(fetchProducts)
	}, [productData])

	//onclick={() => deleteProduct(cartId)}

	// Edit a Product
	const editProductInCart = (e, productId) =>{
		e.preventDefault();
		fetch(`https://mighty-sands-70512.herokuapp.com/cart/${productId}/updated`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
			},
			body: JSON.stringify({
				price: price,
				quantity: quantity,
				totalAmount: price * quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === false){
				getProductsInCart()
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: 'Please try again!'
				})	
			}else{
				getProductsInCart()
				Swal.fire({	
					title: "Success!",
					icon: 'success',
					text: 'Successfully updated!'
				})
				closeEditModal()
			}
		})
	}

	const purchaseProduct = (e) =>{
		e.preventDefault();
		fetch('https://mighty-sands-70512.herokuapp.com/cart/order-successful', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: productName,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(data === false){
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: `Something went wrong. Please try again! `
				})
			}else{
				Swal.fire({
					title: 'Hooray!',
					icon: 'success',
					text: `Successfully purchased ${quantity} pieces of ${productName} worth Php ${price * quantity}`,
					footer: '<a href="https://all-resto-e-commerce-noel-john.vercel.app/orders">Go to My Orders</a>'
				})
				closeProductModal();
			}
		})
	}

	return (
		<Fragment>
			<div className='padding-bottom-5'>
				<div className='my-4 text-center'>
					<h2>Customer's Cart</h2>
				</div>
				<Table striped bordered hover responsive>
					<thead className="bg-dark text-white">
						<tr className='text-center'>
							<th>ID</th>
							<th>Product ID</th>
							<th>Product Name</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total Amount</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{products}
					</tbody>
				</Table>
			</div>
		{/*Edit a Product Modal*/}
			<Modal show={showEditProductModal} onHide={closeEditModal}>
				<Form onSubmit={e => editProductInCart(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Change Quantity</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control 
								type='number'
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Quantity:</Form.Label>
							<Form.Control 
								type='number'
								value={quantity}
								onChange={e => setQuantity(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Total:</Form.Label>
							<Form.Control 
								type='number'
								value={price * quantity}
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant='secondary' onClick={closeEditModal}>Close</Button>
						<Button className='primary-button-modal' variant='success' type='submit'>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		{/*Purchase a Product from Cart*/}
			<Modal show={showProductModal} onHide={closeProductModal}>
				<Form onSubmit={(e) => purchaseProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Purchasing: {productName} worth &#8369;{price}!</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Quantity:</Form.Label>
							<Form.Control 
								type='number'
								value={quantity}
								onChange={e => setQuantity(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Total:</Form.Label>
							<Form.Control 
								type='number'
								value={price * quantity}
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant='secondary' onClick={closeProductModal}>Cancel</Button>
						<Button className='primary-button-modal' variant='success' type='submit'>Confirm Purchase</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</Fragment>
	)
}