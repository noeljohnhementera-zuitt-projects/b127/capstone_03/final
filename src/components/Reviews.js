// Import bootstrap
import { Button, Card, Row, Col, Container } from 'react-bootstrap';

// import react components
import { Fragment } from 'react';


export default function Reviews () {
	return (
		<Fragment>
			<div className='padding-bottom-5'>
				<Container>
					<div>
						<h1 className='text-center padding-top-5'>Reviews</h1>
						<Row>
							<Col sm={12} md={4} lg={4} className='padding-top-5'>
								<Card className='cardHighlight'>
									<Card.Body>
										<Card.Title><h2>Dina Macuja</h2></Card.Title>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<Card.Text>Absolutely amazing place to eat, we will be making a reservation for our next visit.</Card.Text>
									</Card.Body>
								</Card>
							</Col>
							<Col sm={12} md={4} lg={4} className='padding-top-5'>
								<Card className='cardHighlight'>
									<Card.Body>
										<Card.Title><h2>Benny Bilang</h2></Card.Title>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<Card.Text>Absolutely amazing! The place is beautiful and staff are super friendly and the food is delicious. I love that you get a lot of food as well for the price.</Card.Text>
									</Card.Body>
								</Card>
							</Col>
							<Col sm={12} md={4} lg={4} className='padding-top-5'>
								<Card className='cardHighlight'>
									<Card.Body>
										<Card.Title><h2>Mary Christmas Aguinaldo</h2></Card.Title>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<Card.Text>Everything about this restaurant was great. The place was clean and smelled good. The staff and greeter was very nice. Our waiter was awesome, the staff also worked as a team in bringing and cleaning up our food. The food was fresh and awesome.</Card.Text>
									</Card.Body>
								</Card>
							</Col>
						</Row>
					</div>
				</Container>
			</div>
		</Fragment>
	)
} 