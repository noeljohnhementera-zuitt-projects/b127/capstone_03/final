// First Step Imports
import { Jumbotron, Button, Row, Col, Carousel } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function SiteJumbotron() {
	return (
	<Carousel>
	  <Carousel.Item>
	    <img
	      className="d-block w-100"
	      src="https://picsum.photos/800/400"
	      alt="First slide goes here"
	    />
	    <Carousel.Caption>
	      <h1>First slide label</h1>
	      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
	    </Carousel.Caption>
	  </Carousel.Item>
	  <Carousel.Item>
	    <img
	      className="d-block w-100"
	      src="https://picsum.photos/800/400"
	      alt="Second slide"
	    />

	    <Carousel.Caption>
	      <h3>Second slide label</h3>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	    </Carousel.Caption>
	  </Carousel.Item>
	  <Carousel.Item>
	    <img
	      className="d-block w-100"
	      src="https://picsum.photos/800/400"
	      alt="Third slide"
	    />

	    <Carousel.Caption>
	      <h3>Third slide label</h3>
	      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
	    </Carousel.Caption>
	  </Carousel.Item>
	</Carousel>

	)
}

/*<Row>
		<Col>
			<Jumbotron className='mt-4' align='center'>
				<h1>My First E-commerce Website</h1>
				<p>Products for you, for everyone!</p>
				<Row>
					<Col>
						<Button variant='primary'>Buy Now!</Button>
					</Col>
				</Row>
				<Row className='mt-3'>
					<Col>
						<Button variant='primary'>Browse our Categories!</Button>
					</Col>
				</Row>
			</Jumbotron>
		</Col>
	</Row>*/