import { Row, Col, Card, Button, Modal, Form, Container } from 'react-bootstrap';

import PropTypes from 'prop-types';

import { Link, useParams } from 'react-router-dom';

import Swal from 'sweetalert2';

import { useState, Fragment, useEffect, useContext } from 'react';

import GlobalDataContext from "../GlobalDataContext";

export default function ProductCard ( { productProp } ) {

	const { _id, name, description, price } = productProp;

	const  { user } = useContext(GlobalDataContext);

	const { productId } = useParams();

	const [ showProductModal, setShowProductModal ] = useState(false);
	const openProductModal = () => setShowProductModal(true)
	const closeProductModal = () => setShowProductModal(false)

	const [ productName, setProductName ] = useState('');
	const [ newPrice, setNewPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);

	const addProductToCart = (e) =>{
		e.preventDefault()
			fetch('https://mighty-sands-70512.herokuapp.com/cart/added-cart', {
				method: 'POST',
			    headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
				},
				body: JSON.stringify({
					productId: _id,
					productName: name,
					price: price,
					quantity: quantity
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data === false){
					Swal.fire({
							title: "Something went wrong!",
							icon: 'error',
							text: 'Please try again!'
						})
				}else{
					Swal.fire({
						title: 'Hooray!',
						icon: 'success',
						text: `Successfully purchased ${quantity} pieces of ${name} worth Php ${price * quantity}`,
						footer: '<a href="https://all-resto-e-commerce-noel-john.vercel.app/cart">Go to Cart</a>'
					})
					closeProductModal();
				}
			})
	}

	return (
			(user.customerAccessToken !== null) ?
		<Fragment>
				<Container className='container-card'>
					<Row>
						<Col className='py-4'>
							<Card>
								<Card.Body className='product-image'>
									<Card.Title><h2 className='text-white'>{name}</h2></Card.Title>
									<Link className='btn btn-primary-home-products' to={`/products/${_id}`}>See Details</Link>
									<Button className='btn btn-primary-home-products ml-2' onClick={() => openProductModal()}>Add to Cart</Button>
								</Card.Body>
							</Card>
						</Col>
					</Row>
				</Container>

			{/*Modal when purchasing a product*/}
				<Modal show={showProductModal} onHide={closeProductModal}>
					<Form onSubmit={(e) => addProductToCart(e)}>
						<Modal.Header closeButton>
							<Modal.Title>Adding to Cart: {name} worth &#8369;{price}!</Modal.Title>
						</Modal.Header>

						<Modal.Body>

							<Form.Group>
								<Form.Label>Quantity:</Form.Label>
								<Form.Control 
									type='number'
									value={quantity}
									onChange={e => setQuantity(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Total:</Form.Label>
								<Form.Control 
									type='number'
									value={price * quantity}
								/>
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							<Button variant='secondary' onClick={closeProductModal}>Cancel</Button>
							<Button className='primary-button-modal' variant='success' type='submit'>Add To Cart</Button>
						</Modal.Footer>
					</Form>
				</Modal>
		</Fragment>
		:
		<Fragment>
				<Container className='container-card'>
					<Row>
						<Col className='py-4'>
							<Card>
								<Card.Body className='product-image'>
									<Card.Title><h2 className='text-white'>{name}</h2></Card.Title>
									<Link className='btn btn-primary-home-products' to={`/products/${_id}`}>See Details</Link>
								</Card.Body>
							</Card>
						</Col>
					</Row>
				</Container>
		</Fragment>

	)
}

ProductCard.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}