// First Step Imports
import { Row, Button, Col, Card, Container } from 'react-bootstrap';

import { Fragment } from 'react';
export default function () {
	return (
		<Fragment>
			<div className='bg-warning padding-bottom-5 padding-top-5'>
				<Container>
					<h1 className='text-center'>Why Our Customer Love Us?</h1>
					<Row>
						<Col md={12} lg={4} className='padding-top-5'>
							<Card className='cardHighlight'>
								<Card.Body>
								<div className='text-center'>
									<i class="fas fa-bullseye"></i>
								</div>
									<Card.Title><h2 className='text-center'>One Stop Shop!</h2></Card.Title>
									<Card.Text>All Resto E-Commerce offers a wide-array of menus from static menu, beverages, desserts and more! Some categories that we offer are appetizers, pastas, sandwiches to sides and so much more!</Card.Text>
								</Card.Body>
							</Card>				
						</Col>
						<Col md={12} lg={4} className='padding-top-5'>
							<Card className='cardHighlight'>
								<Card.Body>
								<div className='text-center'>
									<i class="fas fa-store-slash"></i>
								</div>
									<Card.Title><h2 className='text-center'>Hassle-Free!</h2></Card.Title>
									<Card.Text>Shop at the comfort of your home. We will take care of the rest!</Card.Text>
								</Card.Body>
							</Card>				
						</Col>
						<Col md={12} lg={4} className='padding-top-5'>
							<Card className='cardHighlight'>
								<Card.Body>
								<div className='text-center'>
									<i class="fas fa-check-circle"></i>
								</div>
									<Card.Title><h2 className='text-center'>Trusted by Many!</h2></Card.Title>
									<Card.Text>All of our dishes are made with care and love that is why We always do our very best to deliver the tastiest and high-quality dishes to our lovely customers.</Card.Text>
								</Card.Body>
							</Card>				
						</Col>
					</Row>
				</Container>
			</div>
		</Fragment>
	)
}